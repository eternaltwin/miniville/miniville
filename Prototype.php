<!DOCTYPE html>
	<?php
		include "BDD.php";
		include "class/ville.php";
		$Maire = 'Piebleu';
		$Ville = new Ville($bdd,$Maire);
	?>
<html lang="fr">
	<head>
		<meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>
		<title><?= $Ville->ReturnName() ?> - Miniville</title>
		<link href="css/prototype.css" type="text/css" rel="stylesheet"/>
		<script type="text/javascript" src="js/js.js"></script>
		<link href="img/favicon.ico" type="image/x-icon" rel="shortcut icon"/>
	</head>
	<body>
		<div id="global">
			<div onclick="document.location='http://<?= $Ville->ReturnName() ?>.miniville.fr/'" id="header">			
				<div class="swf" id="swf_box"></div>
				<div id="city_name"></div>
			</div>
			<div id="city">
				<div id="lineup">
					<div id="content">
						<?= $Ville->StatutVilleFleurie() ?>
						<div class="swf" id="swf_client">
							<p><strong>Ici Affichage Ville. Le Nom est : <?= $Ville->ReturnName() ?></strong><p>
						</div>
						<div id="bottom">
							<div class="stats">
							<table>
								<thead>
									<tr>
										<th>Population</th>
										<th>Revenus</th>
										<th>Maire</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td><?= $Ville->ReturnPopulation() ?></td>
										<td><?= $Ville->ReturnMoney() ?></td>
										<td><?= $Ville->ReturnMaire() ?></td>
									</tr>
								</tbody>
							</table>
								<dl class="list">
									<div class="space5"></div>
									<dt onmouseover="mt.js.Tip.show(this,'Vos habitants ont besoin de travail ! Augmentez l\'industrie pour conserver un faible taux de chomage.',null)" onmouseout="mt.js.Tip.hide()">Chomage</dt>
									<dd class="ok"><?= $Ville->ReturnPourCentChomage() ?>%</dd>
									<dt onmouseover="mt.js.Tip.show(this,'Améliorez le réseau de transport pour que vos habitants puissent se déplacer facilement.',null)" onmouseout="mt.js.Tip.hide()">Transport</dt>
									<dd class="ok"><?= $Ville->ReturnPourCentTransport() ?>%</dd>
									<dt onmouseover="mt.js.Tip.show(this,'Vos habitants veulent se sentir en sécurité ! Augmentez la sécurité pour conserver un faible taux de criminalité.',null)" onmouseout="mt.js.Tip.hide()">Criminalité</dt>
									<dd class="ok"><?= $Ville->ReturnPourCentSecurity() ?>%</dd>
									<dt onmouseover="mt.js.Tip.show(this,'Vive l\'air pur ! Améliorez l\'environnement pour conserver un taux de pollution au plus bas.',null)" onmouseout="mt.js.Tip.hide()">Pollution</dt>
									<dd class="ok"><?= $Ville->ReturnPourCentEnvironement() ?>%</dd>
								</dl>
							</div>
							<div class="clear"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="footer">
			<div id="links">
				<p>Eternal-Twin est un site web non-commercial géré par des amateurs. Son but est d'archiver les jeux de Motion Twin, c'est à dire les garder accessibles même en cas de fermeture du site officiel. Nous ne sommes pas liés à Motion-Twin.</p>
			</div>
		</div>
	</body>
</html>

